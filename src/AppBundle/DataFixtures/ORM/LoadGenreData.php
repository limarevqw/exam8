<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Genre;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class LoadGenreData extends Fixture
{
    const GENRE_ONE = 'Остросюжетные';
    const GENRE_TWO = 'Религии';
    const GENRE_THREE = 'Детские';

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $genre1 = new Genre();
        $genre1->setName(self::GENRE_ONE);
        $manager->persist($genre1);

        $genre2 = new Genre();
        $genre2 ->setName(self::GENRE_TWO);
        $manager->persist($genre2);

        $genre3 = new Genre();
        $genre3->setName(self::GENRE_THREE);
        $manager->persist($genre3);

        $manager->flush();

        $this->addReference(self::GENRE_ONE, $genre1);
        $this->addReference(self::GENRE_TWO, $genre2);
        $this->addReference(self::GENRE_THREE, $genre3);
    }
}