<?php
namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Reader;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class LoadReaderData extends Fixture
{

    const READER_ONE = 'Лимарев Максим Евгеньевич';
    const READER_TWO = 'Стартов Дмитрий Александрович';
    const READER_THREE = 'Владимиров Александр Сергеевич';

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $reader1 = new Reader();
        $reader1->setFio(self::READER_ONE);
        $reader1->setAddress("Токтогула 28");
        $reader1->setPassportID("AN7854895");
        $reader1->setLibraryCard($reader1->getPassportID().$this->getRandomNumber());
        $manager->persist($reader1);

        $reader2 = new Reader();
        $reader2 ->setFio(self::READER_TWO);
        $reader2->setAddress("Московская 40");
        $reader2->setPassportID("AN7854457");
        $reader2->setLibraryCard($reader2->getPassportID().$this->getRandomNumber());
        $manager->persist($reader2);

        $reader3 = new Reader();
        $reader3->setFio(self::READER_THREE);
        $reader3->setAddress("Тыныстанова 54");
        $reader3->setPassportID("AN7854157");
        $reader3->setLibraryCard($reader3->getPassportID().$this->getRandomNumber());
        $manager->persist($reader3);

        $manager->flush();

        $this->addReference(self::READER_ONE, $reader1);
        $this->addReference(self::READER_TWO, $reader2);
        $this->addReference(self::READER_THREE, $reader3);
    }

    public function getRandomNumber()
    {
        return rand(10,20);
    }
}