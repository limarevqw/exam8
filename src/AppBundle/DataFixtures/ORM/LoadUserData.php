<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\User;
use Doctrine\Bundle\FixturesBundle\ORMFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadUserData implements ORMFixtureInterface
{
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $user1 = new User();
        $user1
            ->setUsername('admin@some.com')
            ->setEnabled(true)
            ->setEmail('original@some.com')
            ->setPlainPassword('123321')
            ->setSuperAdmin(true)
        ;
        $manager->persist($user1);
        $manager->flush();
    }
}