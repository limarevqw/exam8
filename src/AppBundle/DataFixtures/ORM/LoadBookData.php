<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Book;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadBookData extends Fixture implements DependentFixtureInterface
{
    private static $books = [];
    private static $booksName = [];

    const BOOK_1 = '«Книжная лавка» Крейг Маклей';
    const BOOK_2 = 'Мастер и Маргарита Коллекционное иллюстрированное издание.';
    const BOOK_3 = 'Колыбельная звезд Карен Уайт';
    const BOOK_4 = 'Разрушенная Тери Терри';
    const BOOK_5 = 'Накануне премьеры фильма «Собибор» вышел роман «Собибор: восстание в лагере смерти»';

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $namesBooks[] = [
            'name' => self::BOOK_1,
            'author' => 'Лимарев Максим Евгеньевич',
            'thumb' => '1.jpg',
            'genre' => $this->getReference(LoadGenreData::GENRE_ONE)
        ];
        $namesBooks[] = [
            'name' => self::BOOK_2,
            'author' => 'Лимарев Максим Евгеньевич',
            'thumb' => '2.jpg',
            'genre' => $this->getReference(LoadGenreData::GENRE_THREE)
        ];
        $namesBooks[] = [
            'name' => self::BOOK_3,
            'author' => 'Лимарев Максим Евгеньевич',
            'thumb' => '3.jpg',
            'genre' => $this->getReference(LoadGenreData::GENRE_TWO)
        ];
        $namesBooks[] = [
            'name' => self::BOOK_4,
            'author' => 'Лимарев Максим Евгеньевич',
            'thumb' => '4.jpg',
            'genre' => $this->getReference(LoadGenreData::GENRE_TWO)
        ];
        $namesBooks[] = [
            'name' => self::BOOK_5,
            'author' => 'Лимарев Максим Евгеньевич',
            'thumb' => '5.jpg',
            'genre' => $this->getReference(LoadGenreData::GENRE_ONE)
        ];
        $namesBooks[] = [
            'name' => '«Время – убийца» Мишель Бюсси',
            'author' => 'Лимарев Максим Евгеньевич',
            'thumb' => '6.jpg',
            'genre' => $this->getReference(LoadGenreData::GENRE_THREE)
        ];
        $namesBooks[] = [
            'name' => 'Девушка в тумане Донато Карризи',
            'author' => 'Лимарев Максим Евгеньевич',
            'thumb' => '7.jpg',
            'genre' => $this->getReference(LoadGenreData::GENRE_THREE)
        ];
        $namesBooks[] = [
            'name' => 'Фантомная память Франк Тилье',
            'author' => 'Лимарев Максим Евгеньевич',
            'thumb' => '8.jpg',
            'genre' => $this->getReference(LoadGenreData::GENRE_TWO)
        ];
        $namesBooks[] = [
            'name' => 'Гравити Фолз. Графический роман. Выпуск 3',
            'author' => 'Лимарев Максим Евгеньевич',
            'thumb' => '9.jpg',
            'genre' => $this->getReference(LoadGenreData::GENRE_THREE)
        ];
        $namesBooks[] = [
            'name' => 'Американские боги Нил Гейман Уникальное иллюстрированное издание!',
            'author' => 'Лимарев Максим Евгеньевич',
            'thumb' => '10.jpg',
            'genre' => $this->getReference(LoadGenreData::GENRE_ONE)
        ];

        foreach ($namesBooks as $item) {
            $book = new Book();
            $genre = $item['genre'];
            $book
                ->setName($item['name'])
                ->setAuthor($item['author'])
                ->setThumb($item['thumb'])
                ->addJenre($genre)
            ;

            $genre->addBook($book);

            $manager->persist($book);
            $bookIdent = $item['name'];
            $this->addReference($bookIdent, $book);
            self::$books[] = $this;
            self::$booksName[] = $bookIdent;

            $manager->flush();
        }

    }

    public function getDependencies()
    {
        return [
            LoadGenreData::class
        ];
    }

    public static function getBooks() {
        return self::$books;
    }
}