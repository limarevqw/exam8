<?php
namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Bid;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadBidData extends Fixture implements DependentFixtureInterface
{
    private static $bids = [];

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $dataTime = new \DateTime();

        $namesBooks[] = [
            'status' => true,
            'return_date' => $dataTime->modify("+1 month"),
            'date_creation' => $dataTime,
            'reader_id' => $this->getReference(LoadReaderData::READER_ONE),
            'book_id' => $this->getReference(LoadBookData::BOOK_1),
        ];
        $namesBooks[] = [
            'status' => true,
            'return_date' => $dataTime->modify("+4 month"),
            'date_creation' => $dataTime,
            'reader_id' => $this->getReference(LoadReaderData::READER_ONE),
            'book_id' => $this->getReference(LoadBookData::BOOK_2),
        ];
        $namesBooks[] = [
            'status' => true,
            'return_date' => $dataTime->modify("+6 month"),
            'date_creation' => $dataTime,
            'reader_id' => $this->getReference(LoadReaderData::READER_THREE),
            'book_id' => $this->getReference(LoadBookData::BOOK_3),
        ];
        $namesBooks[] = [
            'status' => true,
            'return_date' => $dataTime->modify("+5 month"),
            'date_creation' => $dataTime,
            'reader_id' => $this->getReference(LoadReaderData::READER_ONE),
            'book_id' => $this->getReference(LoadBookData::BOOK_4),
        ];
        $namesBooks[] = [
            'status' => true,
            'return_date' => $dataTime->modify("+8 month"),
            'date_creation' => $dataTime,
            'reader_id' => $this->getReference(LoadReaderData::READER_THREE),
            'book_id' => $this->getReference(LoadBookData::BOOK_5),
        ];

        foreach ($namesBooks as $item) {
            $bid = new Bid();
            $bid
                ->setStatus($item['status'])
                ->setReturnDate($item['return_date'])
                ->setDateCreation($item['date_creation'])
                ->setReader($item['reader_id'])
                ->setBook($item['book_id'])
            ;

            $manager->persist($bid);
            self::$bids[] = $this;

            $manager->flush();
        }
    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    function getDependencies()
    {
        return [
            LoadReaderData::class,
            LoadBookData::class
        ];
    }

    public static function getBids() {
        return self::$bids;
    }
}