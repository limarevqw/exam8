<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Bid;
use AppBundle\Entity\Book;
use AppBundle\Entity\Genre;
use AppBundle\Entity\Reader;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class MainController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        $data = $this->getData();
        return $this->render("@App/main/index.html.twig", $data);
    }

    /**
     * @Route("/category/{id}")
     * @param int $id
     * @return Response
     */
    public function categoryAction(int $id)
    {
        $genre = $this
            ->getDoctrine()
            ->getRepository(Genre::class)
            ->find($id);

        $books = $this
            ->getDoctrine()
            ->getRepository(Book::class)
            ->genreBooks($genre);

        $data = $this->getData();
        $data['books'] = $books;

        return $this->render("@App/main/index.html.twig", $data);
    }

    /**
     * @Route("/account-books")
     * @return Response
     */
    public function accountBooksAction()
    {
        $data = $this->getData();
        return $this->render("@App/main/accountBooks.html.twig", $data);
    }

    /**
     * @Route("/get-reader-books")
     * @param Request $request
     * @return Response
     */
    public function getReaderBooksAction(Request $request)
    {
        $libraryCard = $request->get('library_card');

        $data = "";
        $status = false;
        $message = "Нечего не найдено!";

        $reader = $this->getDoctrine()
            ->getRepository(Reader::class)
            ->findReaderToNameLibraryCard($libraryCard);

        if ($reader) {
            $bids = $this
                ->getDoctrine()
                ->getRepository(Bid::class)
                ->readerBooks($reader[0]);

            $data = $this->render("@App/main/getReaderBooks.html.twig", [
                'bids' => $bids
            ])->getContent();

            $message = "Список ваших книг!";
            $status = true;
        }
        return new JsonResponse([
            'status' => $status,
            'books' => $data,
            'message' => $message,

        ]);
    }

    /**
     * @Route("/account-registration")
     * @return Response
     * @throws \Exception
     */
    public function accountRegistrationAction()
    {
        $data = $this->getData();
        return $this->render("@App/main/accountRegistration.html.twig", $data);
    }

    /**
     * @Route("/show-book/{id}")
     * @param int $id
     * @return Response
     */
    public function showBookAction(int $id)
    {
        $book = $this
            ->getDoctrine()
            ->getRepository(Book::class)
            ->find($id);

        $data = $this->getData();
        $data['book'] = $book;
        return $this->render("@App/main/showBook.html.twig", $data);
    }

    /**
     * @Route("/new-account-registration")
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function newAccountRegistrationAction(Request $request)
    {
        $data = "";
        $status = false;
        $fio = $request->get('fio');
        $address = $request->get('address');
        $passportID = $request->get('passportID');
        $library_card = $passportID.rand(10, 20);
        $readerPassportID = $this->getDoctrine()
            ->getRepository(Reader::class)
            ->findReaderToPassportID($passportID);

        $em = $this->getDoctrine()->getManager();


        if ($readerPassportID) {
            return new JsonResponse([
                'status' => $status,
                'data' => $data,
                'message' => "Ошибка! с таким паспортом пользователь у же есть в системе!",

            ], 403);
        }

        $reader = new Reader();
        $reader->setPassportID($passportID);
        $reader->setFio($fio);
        $reader->setAddress($address);
        $reader->setLibraryCard($library_card);
        $em->persist($reader);
        $em->flush();

        $data = $reader->getLibraryCard();
        $status = true;
        $message = "Читательский билет успешно создан! номер читательского билета ".$data;


        return new JsonResponse([
            'status' => $status,
            'data' => $data,
            'message' => $message,

        ]);
    }

    public function getData()
    {
        $genres = $this
            ->getDoctrine()
            ->getRepository(Genre::class)
            ->findAll();

        $books = $this
            ->getDoctrine()
            ->getRepository(Book::class)
            ->findAll();

        return [
            'genres' => $genres,
            'books' => $books,
        ];
    }

    /**
     * @Route("/{_locale}", requirements = {"_locale" : "en|ru"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function changeLocal(Request $request)
    {
        return $this->redirect($request->headers->get('referer'));
    }

}
