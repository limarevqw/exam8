<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Book
 *
 * @ORM\Table(name="book")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BookRepository")
 * @Vich\Uploadable
 */
class Book
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="author", type="string", length=255)
     */
    private $author;

    /**
     * @var string
     *
     * @ORM\Column(name="thumb", type="string", nullable=true)
     */
    private $thumb;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="thumb_file", fileNameProperty="thumb")
     *
     * @var File
     */
    private $thumbFile;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Genre", mappedBy="books")
     */
    private $jenres;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Bid", mappedBy="book")
     */
    protected $bids;

    /**
     * Book constructor.
     */
    public function __construct()
    {
        $this->jenres = new ArrayCollection();
        $this->bids = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Book
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set author
     *
     * @param string $author
     *
     * @return Book
     */
    public function setAuthor($author)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return string
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param string $thumb
     * @return Book
     */
    public function setThumb($thumb)
    {
        $this->thumb = $thumb;
        return $this;
    }

    /**
     * @return string
     */
    public function getThumb()
    {
        return $this->thumb;
    }

    /**
     * @param File|UploadedFile
     * @return Book
     */
    public function setThumbFile($thumbFile)
    {
        $this->thumbFile = $thumbFile;
        return $this;
    }

    /**
     * @return File
     */
    public function getThumbFile()
    {
        return $this->thumbFile;
    }

    /**
     * @param mixed $bids
     * @return Book
     */
    public function setBids($bids)
    {
        $this->bids = $bids;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBids()
    {
        return $this->bids;
    }

    /**
     * Add jenre
     *
     * @param \AppBundle\Entity\Genre $jenre
     *
     * @return Book
     */
    public function addJenre(\AppBundle\Entity\Genre $jenre)
    {
        $this->jenres[] = $jenre;

        return $this;
    }

    /**
     * Remove jenre
     *
     * @param \AppBundle\Entity\Genre $jenre
     */
    public function removeJenre(\AppBundle\Entity\Genre $jenre)
    {
        $this->jenres->removeElement($jenre);
    }

    /**
     * Get jenres
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getJenres()
    {
        return $this->jenres;
    }

    /**
     * Add bid
     *
     * @param \AppBundle\Entity\Bid $bid
     *
     * @return Book
     */
    public function addBid(\AppBundle\Entity\Bid $bid)
    {
        $this->bids[] = $bid;

        return $this;
    }

    /**
     * Remove bid
     *
     * @param \AppBundle\Entity\Bid $bid
     */
    public function removeBid(\AppBundle\Entity\Bid $bid)
    {
        $this->bids->removeElement($bid);
    }

    /**
     * @param mixed $jenres
     * @return Book
     */
    public function setJenres($jenres)
    {
        $this->jenres = $jenres;
        return $this;
    }
}
