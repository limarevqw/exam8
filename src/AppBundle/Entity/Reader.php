<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Reader
 *
 * @ORM\Table(name="reader")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ReaderRepository")
 */
class Reader
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="fio", type="string", length=255)
     */
    private $fio;

    /**
     * @var string
     *
     * @ORM\Column(name="adress", type="string", length=255)
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="passportID", type="string", length=255, unique=true)
     */
    private $passportID;

    /**
     * @var string
     *
     * @ORM\Column(name="library_card", type="string", length=255, unique=true)
     */
    private $libraryCard;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Bid", mappedBy="reader")
     */
    protected $bids;

    /**
     * Reader constructor.
     */
    public function __construct()
    {
        $this->bids = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fio
     *
     * @param string $fio
     *
     * @return Reader
     */
    public function setFio($fio)
    {
        $this->fio = $fio;

        return $this;
    }

    /**
     * Get fio
     *
     * @return string
     */
    public function getFio()
    {
        return $this->fio;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Reader
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set passportID
     *
     * @param string $passportID
     *
     * @return Reader
     */
    public function setPassportID($passportID)
    {
        $this->passportID = $passportID;

        return $this;
    }

    /**
     * Get passportID
     *
     * @return string
     */
    public function getPassportID()
    {
        return $this->passportID;
    }

    /**
     * Set libraryCard
     *
     * @param string $libraryCard
     *
     * @return Reader
     */
    public function setLibraryCard($libraryCard)
    {
        $this->libraryCard = $libraryCard;

        return $this;
    }

    /**
     * Get libraryCard
     *
     * @return string
     */
    public function getLibraryCard()
    {
        return $this->libraryCard;
    }

    /**
     * @param mixed $bids
     * @return Reader
     */
    public function setBids($bids)
    {
        $this->bids = $bids;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBids()
    {
        return $this->bids;
    }

    /**
     * Add bid
     *
     * @param \AppBundle\Entity\Bid $bid
     *
     * @return Reader
     */
    public function addBid(\AppBundle\Entity\Bid $bid)
    {
        $this->bids[] = $bid;

        return $this;
    }

    /**
     * Remove bid
     *
     * @param \AppBundle\Entity\Bid $bid
     */
    public function removeBid(\AppBundle\Entity\Bid $bid)
    {
        $this->bids->removeElement($bid);
    }
}
